var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// 
var dbo = require('./app/database/mysql');
var validateData = require('./app/validate');
// 
var index = require('./routes/index');
// 
var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/*
* Set expected routing of the app
*/
app.use('/', index);
// Save a user 
app.post('/saveUser', (req, res) => {
  var feResponse = {'status': 1, 'message': '', 'errCode': ''};
  var saveUserData = {};
  // Go collect the required data and run another validation
  saveUserData.name = validateData.getData(req.body, 'name', '[^a-z\\s\\-]+|\\s{2,}');
  saveUserData.surname = validateData.getData(req.body, 'surname', '[^a-z\\s]+|\\s{2,}');
  saveUserData.id_num = validateData.getData(req.body, 'idNum', '[^0-9]');
  saveUserData.cell = validateData.getData(req.body, 'cell', '[^0-9]+');
  saveUserData.email = validateData.getData(req.body, 'email', '[^a-z0-9\\.\\-\\_\\@]+');
  saveUserData.address = validateData.getData(req.body, 'address', '[^a-z\\s]+|\\s{2,}');
  if(saveUserData.id_num && saveUserData.id_num.length < 13) {
    feResponse.status = 0;
    feResponse.message = 'Ensure your ID Number is 13 digits.';
    res.send(JSON.stringify(feResponse));
    return;
  }
  // console.log('Clean data', saveUserData);
  // Can we try save the information?
  if(saveUserData.name && saveUserData.surname && saveUserData.id_num && saveUserData.cell && saveUserData.email && saveUserData.address) {
    // First see if the ID number has not yet already been added (Could look at setting column to unique)
    // Will need to do the same for email
    dbo("SELECT id FROM users where `id_num`="+saveUserData.id_num+"", function(err, row) {
      // Go ahead and insert if we can
      if(!row.length) {
        dbo("INSERT INTO users SET `name`= '" + saveUserData.name + "', `surname`='" + saveUserData.surname + "', `id_num`= '" + saveUserData.id_num + "', `cell`='" + saveUserData.cell + "', `email`='" + saveUserData.email + "', `address`='" + saveUserData.address + "'", function(err, result) {
          // console.log('Test --- ', err, result);
          if(err) {
            feResponse.status = 0;
            feResponse.message = 'An unexpected error has occured. Please try again later.';
            res.send(JSON.stringify(feResponse));
          } else {
            feResponse.status = 1;
            res.send(JSON.stringify(feResponse));
          }
        });
      } else {
        feResponse.status = 0;
        feResponse.message = 'This user with SA ID Numner '+saveUserData.id_num+' has already been added.';
        res.send(JSON.stringify(feResponse));
      }
    });

  } else {
    feResponse.status = 0;
    feResponse.message = 'Please fill in all the required fields';
    res.send(JSON.stringify(feResponse));
  }
  // res.send(req.body);
});

// Search User
app.post('/searchUser', (req, res) => {
  var feResponse = {'status': 1, 'message': '', 'searchq':'', 'search': []};
  feResponse.searchq = validateData.getData(req.body, 'searchQ', '[^a-z0-9\\@\\.\\-\\_\\s]+|\\s{2,}');
  var dbCols = 'name,surname,id_num as IDNumber,cell,email,address';
  // Try identify what the user is looking for, i.e. ID or name surname?
  // The below can then be enhanceed to allow email
  if(!feResponse.searchq) {
    res.send(JSON.stringify(feResponse));
  } else if(/^[0-9\s]+$/.test(feResponse.searchq)) {
    // console.log('search by Id Number');
    dbo("SELECT " + dbCols +" FROM users where `id_num` LIKE '"+feResponse.searchq+"%'", function(err, row) {
      if(row.length){
        // console.log('Row returned --> ', row);
        feResponse.search = row;
        res.send(JSON.stringify(feResponse));
      } else {
        res.send(JSON.stringify(feResponse));
      }
    });
  } else {
    feResponse.searchq = feResponse.searchq.toLowerCase();
    dbo("SELECT name,surname,id_num as IDNumber,cell,email,address FROM users WHERE LOWER(`name`) LIKE '%"+feResponse.searchq+"%' OR LOWER(`surname`) LIKE '%"+feResponse.searchq+"%'", function(err, row) {
      if(row.length){
        // console.log(row);
        feResponse.search = row;
        res.send(JSON.stringify(feResponse));
      } else {
        res.send(JSON.stringify(feResponse));
      }
    });
  }

});

/*
* Error Handling
*/
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

//Start listening on the below port
app.listen(3000);