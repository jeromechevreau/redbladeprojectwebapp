$(document).ready(function() {
	// User saving vars
	var $frmSbmitBtn = $('a#frm-submit-saveuser');
	var $formTarget = $('form#saveuser');
	var btnClicked = false;
	var btnOriTxt = '';
	var $alertBarEl = $('div#alert-bar');
	// Search user vars
	var $searchFld = $('input#search');
	var $searchResContainer = $('table#userlookupListing > tbody')
	var searchAjxReq = '';
	var searchTM = 0;
	// 
	var autoHideAlertT = 0;
	// 
	var validator = $formTarget.validate();
	if($('button#useMapForAddress').length) {
		$('button#useMapForAddress').off('click').on('click', function(){
			includeGMapFunctionality();
			$(this).off('click');
		});
	}
	// debugger;
	/*
	* SAVE USER
	*/
	if($frmSbmitBtn.length && $formTarget.length) {
		// Hold on the original text of the button
		btnOriTxt = $frmSbmitBtn.html();
		// Handle the click event
		$frmSbmitBtn.off('click').on('click', function(){
			if(btnClicked){
				return false;
			}
			// 
			if(!validator.form()){
				$alertBarEl.removeClass('alert-success').addClass('alert-danger').removeClass('hide').html('Please ensure that all fields are filled in.');
				autoHideAlertT = setTimeout(function(){hideAlertBar();}, 3000);
				return;
			}
			// debugger;
			// 
			hideAlertBar();
			// 
			$frmSbmitBtn.html('<i>Saving...</i>');
			btnClicked = true;
			// 
			$.ajax({
				"url": "/saveUser",
				"dataType": "json",
				"method" : "POST",
				"data" : $formTarget.serialize(),
				success : function(respData) {
					btnClicked = false;
					$frmSbmitBtn.html(btnOriTxt);
					if(!respData.status) {
						$alertBarEl.removeClass('alert-success').addClass('alert-danger').removeClass('hide').html((respData.message? respData.message : 'An unexpected error occured. Please try again later.'));
						autoHideAlertT = setTimeout(function(){hideAlertBar();}, 3000);
					} else {
						$alertBarEl.removeClass('alert-danger').addClass('alert-success').removeClass('hide').html('User successfully added.');
						// Clear all fields value
						$formTarget.find('input').each(function(){
							$(this).val('');
						});
						autoHideAlertT = setTimeout(function(){hideAlertBar();}, 2000);
					}
					// 
				},
				error : function(error){
					console.log("Error on submit", error);
					btnClicked = false;
					$frmSbmitBtn.html(btnOriTxt);
					// 
					$alertBarEl.removeClass('hide').removeClass('alert-success').addClass('alert-danger').html('');
					// 
					autoHideAlertT = setTimeout(function(){hideAlertBar();}, 3000);
				}
			});

		});
	}
	/*
	* handle the search
	*/
	if($searchFld.length) {
		$searchFld.off('keyup').on('keyup', function(e) {
			// console.log('earch used?? ', e.keyCode);
			if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 46 || e.keyCode === 8) {
				if(searchAjxReq) {
					searchAjxReq.abort();
				}
				if(searchTM) {
					clearTimeout(searchTM);
					searchTM = 0;
				}
				if(!$searchFld.val()){
					$searchResContainer.html('');
				} else {
					searchTM = setTimeout(function(){
						sendSearchQuery();	
					}, 250);
				}
			}
			
		});
	}
	// SEARCH Request handler
	function sendSearchQuery () {
		searchAjxReq = $.ajax({
			"url": "/searchUser",
			"dataType": "json",
			"method" : "POST",
			"data" : {
				'searchQ': $searchFld.val()
			},
			success : function(respData) {
				if(respData.status && typeof respData.search == 'object') {
					$searchResContainer.html('');
					var searchData = respData.search;
					for(var i=0, len=searchData.length; i<len; i++) {
						$searchResContainer.append('<tr> <td>' + searchData[i].name +'</td> <td>' + searchData[i].surname +'</td> <td>' + searchData[i].IDNumber +'</td> <td>' + searchData[i].email +'</td> <td>' + searchData[i].cell +'</td> <td>' + searchData[i].address +'</td> </tr>');
					}
				}
			},
			error : function(error){
				console.log("Error on submit", error);
				// 
			}
		});
	}
	// Quick function to allow hide the alert bar (to be moved into a site main script later)
	function hideAlertBar(){
		$alertBarEl.addClass('hide').html('');
		if(autoHideAlertT) {
			clearTimeout(autoHideAlertT);
		}
	}
	
});
// Enable google map api
function includeGMapFunctionality(){
	if(typeof google === 'undefined'){
		$('body').append('<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD4zOnh8XBjiJaw2yaaGCbXyk0P_inT14&amp;libraries=places&amp;callback=initAutocomplete" async="" defer=""></script>');
	}
}

function initAutocomplete(){
	// 
	if($('div#maplocationcontainer').length){
		$('div#maplocationcontainer').removeClass('hide');
	}
	// 
	var map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -33.91928, lng: 18.42533},
		zoom: 13,
		mapTypeId: 'roadmap'
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Try get the current location of the user (only if it is supported by the browser)
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			map.setCenter(pos);
		}, function() {
			console.log('There was an issue getting your location');
		});
	}

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

	var markers = [];
	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Allow what the user used for confirmation to be used - reli on gmap for confirmation
		$('input[name="address"]', 'form#saveuser').eq(0).val(places[0].formatted_address);
		// console.log(places[0]);

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});
		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);
	});
}