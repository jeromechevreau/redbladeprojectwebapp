var validate ={};
// 
validate.getData = function(dataSrc, target, validation) {
	var regexp = (validation ? new RegExp(validation, 'gi') : '');
	var value = '';
	if(dataSrc.hasOwnProperty(target)) {
		value = dataSrc[target].replace(regexp, '');
	}
	return value;
};
module.exports = validate;