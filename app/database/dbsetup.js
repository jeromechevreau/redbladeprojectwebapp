var mysql  = require('mysql');
var dbName = 'redbladeProject001';
// DB config - to be later move a centralised configuration file
var dbConfig = {
	'host': '127.0.0.1',
	'user': 'root',
	'password': ''
};
// 
var testDBTable = "CREATE TABLE IF NOT EXISTS `users` ("  
+"`id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,"
+"`name` varchar(100) COLLATE utf8_bin DEFAULT NULL,"
+"`surname` varchar(100) COLLATE utf8_bin DEFAULT NULL,"
+"`id_num` bigint(20) UNSIGNED DEFAULT NULL,"
+"`cell` bigint(20) UNSIGNED DEFAULT NULL,"
+"`email` varchar(255) COLLATE utf8_bin DEFAULT NULL,"
+"`address` varchar(255) COLLATE utf8_bin NOT NULL"
+") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
// 
var connection = mysql.createConnection(dbConfig);
connection.connect(function(err) {
	// console.log('connection open?');
	if(err !== null) {
		console.log("Yoopsie, there has been an error while trying to connect: " + err);
		return;
	}
	// 
	createDataBase();
});
// 
function createDataBase(){
	connection.query("CREATE DATABASE IF NOT EXISTS " + dbName + " CHARACTER SET utf8 COLLATE utf8_bin", function(){
		createDBTable();
	});
}
// 
function createDBTable(){
	// Close connection
	connection.end();
	// Add db config so we can create table
	dbConfig.database = dbName;
	// Open new connection as we have now the Database created
	connection = mysql.createConnection(dbConfig);
	connection.query(testDBTable, function(){
		connection.end();
	});
}