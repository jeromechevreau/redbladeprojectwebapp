var mysql  = require('mysql');
// DB config - to be later move a centralised configuration file
var dbConfig = {
	'host': '127.0.0.1',
	'user': 'root',
	'password': '',
	'database': 'redbladeProject001'
};
// 
var dbConnection = function mysqlDbConnection(sql, next) {
	// 
	var connection = mysql.createConnection(dbConfig);
	connection.connect(function(err){
		if(err !== null) {
			// console.log("Yoopsie, there has been an error while trying to connect: " + err.stack);
			console.log("Yoopsie, there has been an error while trying to connect: " + err);
			return;
		}
		// Run Query Once the connection has been successful
		var dbQuery = connection.query(sql, function(err, results) {
			// Close current connection
			connection.end();
			if(err){
				console.log('Query Error-> ' + err);
				next(err, false);
				return;
			}
			// console.log('Query here ', sql, rows);
			next(err, results);
		});
		console.log('Test Query - ', dbQuery.sql);

	});

};

module.exports = dbConnection;
